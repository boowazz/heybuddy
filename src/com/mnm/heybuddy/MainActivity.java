package com.mnm.heybuddy;

import org.apache.cordova.DroidGap;
import android.os.Bundle;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseInstallation;
import com.parse.PushService;


public class MainActivity extends DroidGap {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.loadUrl("file:///android_asset/www/index.html");
        super.setIntegerProperty("loadUrlTimeoutValue", 10000);
        
        Parse.initialize(this, "o6UmKN6Q81i9tHglqCtWVWMbz437ssAzNLrWuvwI", "yJFQsCi35UMsy7HTzPwROk7CtisXhbvtMJ8dSnG9"); 
        PushService.setDefaultPushCallback(this, MainActivity.class);
        ParseAnalytics.trackAppOpened(getIntent());
    }
}

