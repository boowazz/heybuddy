jQuery.extend(true, app, {
	message: {

		init: function () {
			console.log('Message loaded');

			var _self = this;

			$(document).on("pagecreate", '#message', function () {
				console.log('pagecreate: #messagelist: This should fire only once.');
				if (app.authentication.is_logged_in() === false) {
					$.mobile.navigate('#home');
				} else {
					_self.listeners();
				}
			});

			$(document).on("pagebeforeshow", '#message', function () {
				if (app.authentication.is_logged_in() === false) {
					$.mobile.navigate('#home');
				}
			});

			$(document).on("pagehide", '#message', function () {
				$('#chatMessages').empty();
				$('.empty-chat').addClass('show');
				if (_self.phono_instance !== null) {
					_self.append('sent', 'I have now disconnected. To chat again please go to the message list and select the job.', function () {
						alert('By leaving the chat, you have disconnected. To chat again please go to the message list and select the job.');
						_self.phono_instance.messaging.send($('#chatObject').attr('data-phono-id-to'),'I have now disconnected. To chat again please go to the message list and select the job.');
					});
				}
			});
		},

		listeners: function () {
			var _self = this;
			$('#submitChat').on('click',function () {

				if ($('#chatObject').attr('data-phono-id-to') !== '') {
					$.mobile.loading("show");
					_self.append('sent', $('.chat-content').val(), function () {
						_self.phono_instance.messaging.send($('#chatObject').attr('data-phono-id-to'),$('.chat-content').val());
						_self.retrieve($('#chatObject').attr('data-object-id'), function () {
							$('.chat-content').val('');
							$.mobile.loading('hide');
						});
					});
				} else {
					navigator.notification.alert(
						'Please wait for all users to connect',
						null,
						app.name,
						'Ok'
					);
					$.mobile.loading("hide");
				}
			});
		},

		load_thread: function (jobId) {

			var _self = this;

			$.mobile.loading("show");

			$('.empty-chat').removeClass('show');

			_self.phono_instance = null;

			_self.retrieve(jobId, function () {

				_self.phono_instance = $.phono({
					apiKey: app.phono_api_key,
					audio: {
						type: 'none'
					},
					onReady: function (event) {
						_self.phono_key = this.sessionId;
						_self.setup_push(jobId, this.sessionId);
						$.mobile.navigate('#message');
						$.mobile.loading("show");
						console.log('waiting for reply');
						$.mobile.loading( "show", {
							text: "Waiting for reply."
						});
					},
					messaging: {
						onMessage: function (event) {
							var message = event.message;
							var from = message.from;
							$('#chatObject').attr('data-phono-id-to', from);
							_self.append('received', message.body, function () {
								console.log(message.body);
								_self.retrieve($('#chatObject').attr('data-object-id'), function () {})
							});
						}
					}
				});
			})

		},

		received_push: function (push_object) {

			var _self = this;

			$('.empty-chat').removeClass('show')

			_self.retrieve(push_object.jobID, function () {

				$('#chatObject').attr('data-phono-id-to', push_object.phonoSessionID);
				$('#chatObject').attr('data-object-id', push_object.jobID);

				_self.phono_instance = null;

				_self.phono_instance = $.phono({
					apiKey: app.phono_api_key,
					audio: {
						type: 'none'
					},
					onReady: function (event) {

						_self.phono_key = this.sessionId;

						$.mobile.loading("hide");

						_self.append('sent', "CONNECTED", function () {
							_self.phono_instance.messaging.send($('#chatObject').attr('data-phono-id-to'),"CONNECTED");
							_self.retrieve($('#chatObject').attr('data-object-id'), function () {})
						});

					},
					messaging: {
						onMessage: function (event) {
							_self.append('received', event.message.body, function () {
								_self.retrieve($('#chatObject').attr('data-object-id'), function () {})
							});
						}
					}
				});
			});
		},


		retrieve: function (jobId, callback) {

			/*
			* Refresh the message thread. load in messages or establish localstorage object.
			* */

			if (localStorage.getItem('chat_'+jobId) !== null) {
				try{
					var object = localStorage.getItem("chat_"+jobId);
				} catch (e) {
					console.log(JSON.stringify(e))
				}

				console.log(object);

				var object_parse = JSON.parse(object);

				console.log(object_parse.objectId)
				console.log(object_parse.messages.length)

				var messages = object_parse.messages,
					context;
					

				console.log('retrieve phono id plz', $('#chatObject').attr('data-phono-id-to'))
$.mobile.loading("show");

				if (messages.length > 0) {
					messages = _(messages).sortBy(function(message) {
						return message.timestamp;
					});

					context = {
						objectId: object_parse.objectId,
						jobTitle: object_parse.jobTitle,
						recipient: {
							name: object_parse.recipient.name
						},
						messages: messages,
						phonoId: $('#chatObject').attr('data-phono-id-to')
					};

				} else {
					context = {
						objectId: object_parse.objectId,
						jobTitle: object_parse.jobTitle,
						recipient: {
							name: object_parse.recipient.name
						},
						messages: [],
						phonoId: $('#chatObject').attr('data-phono-id-to')
					};
				}
				var source = $('#message-page-template').html();

				var template = Handlebars.compile(source);

				var html = template(context);

				$("#chatMessages").html(html);

				$.mobile.loading("hide");

				callback();

			} else {
				var job_object = Parse.Object.extend("Job");
				var query = new Parse.Query(job_object);
				query.include("user");

				console.log(jobId);

				query.equalTo('objectId', jobId).find({
					success: function(results) {

						var data;

						for(var i = 0; i < results.length; i++) {

							var result = results[i];
							var user  = result.get('user').toJSON();
							var job = results[i].toJSON();

							console.log(JSON.stringify(job));
							console.log(JSON.stringify(user));

							data = {
								jobTitle: job.jobTitle,
								objectId: job.objectId,
								recipient: {
									name: user.name
								},
								messages:[],
								phonoId: $('#chatObject').attr('data-phono-id-to')
							};
						}

						console.log('new localstorage');

						console.log(JSON.stringify(data));

						var source = $('#message-page-template').html();

						var template = Handlebars.compile(source);

						var html = template(data);

						$("#chatMessages").html(html);

						localStorage.setItem('chat_'+job.objectId, JSON.stringify(data));

						callback();

					},

					//if Something goes wrong
					error: function(e, error) {
						$.mobile.loading("hide");
						console.log(e);
					}
				});
			}
		},


		setup_push: function (jobId, sessionId) {

			var jobOb = Parse.Object.extend("Job");
			var jobUser = Parse.Object.extend("User");
			var query = new Parse.Query(jobOb);
			var _self = this;

			query.include("user");

			query.equalTo('objectId', jobId).find({
				success: function(results) {

					var pushObject = {};

					for(var i = 0; i < results.length; i++) {

						var result = results[i];
						var jobUser  = result.get('user').toJSON();
						var job = results[i].toJSON();
						var channel = "custom_" + jobUser.objectId;
						var me = Parse.User.current();

						var data = {
							alert: me.get('name') + " would like to talk about " + job.jobTitle,
							'jobID': jobId,
							'userID': me.id,
							'phonoSessionID': sessionId
						};

						_self.push(channel, data);
					}

				},

				//if Something goes wrong
				error: function(e, error) {
					$.mobile.loading("hide");
					console.log(e);
				}

			});

		},

		push: function (channel, data) {
			Parse.Push.send({
				channels: [ channel ],
				data: data
			}, {
				success: function(test) {
					// Push was successful
					console.log(JSON.stringify(test));


				},
				error: function(error) {
					// Handle error
					navigator.notification.alert(
						'Something went wrong.',
						null,
						app.name,
						'Ok'
					);
					console.log('!!Push test error!!')
				}
			});
		},

		append: function (chatType, content, callback) {


			console.log('append go')

			var _self = this,
				message = {
					timestamp:new moment().valueOf(),
					type: chatType,
					content: content
				},
				storage_object;

			var current_id = $('#chatObject').attr('data-object-id');

			storage_object = localStorage.getItem('chat_'+current_id);

			console.log(storage_object)

			storage_object.messages.push(message);

			if (storage_object.messages.length > 20) {
				storage_object.messages.shift();
			}

			localStorage.setItem('chat_' + current_id, JSON.stringify(storage_object));
			console.log(JSON.stringify(storage_object))

			callback();

		}
	}
});



