//let's define some constants!
var app = {
    parse_app_id: "o6UmKN6Q81i9tHglqCtWVWMbz437ssAzNLrWuvwI",
    parse_js_id: "0HMBdVJgrpYOc0yAQDdUXFQqMtLMB3DPOAhMEOND",
	parse_client_key: "yJFQsCi35UMsy7HTzPwROk7CtisXhbvtMJ8dSnG9",
    fb_app_id: "717765138254866",
    phono_api_key: "bc1cdeaaab263d8ee2ec6eb6c26bdf1d",
	parse_master_key: "yUrAuFYaNnhBDERVawdCWK7bgXRHYJqKvVEojdFJ",
	name: 'HeyBuddy',
	globals: function () {


		var _self = this;

		console.log('Initial app object established ^_*')

		//setup Parse. If we gon' push, how we gon'
		Parse.initialize(app.parse_app_id, app.parse_js_id);

		_self.date_formats = {
			default: "DD/MM/YY",
			long: "DD/MM",
			chat: "DD/MM HH:mm"
		};

		Handlebars.registerHelper("formatDate", function(datetime, format) {
			if (moment) {

				//put in your 'NOW' logic
				var f = _self.date_formats[format];
				return moment(datetime).format(f);
			}
			else {
				return datetime;
			}
		});

		$('.js-logout').on('click', function (e) {
			e.preventDefault();
			app.authentication.logout();
			console.log('logout clicked!')
		});

		if (app.on_mobile() === true) {
			window.plugins.webintent.hasExtra(
				"com.parse.Data",
				function(has) {
					if(has) {
						window.plugins.webintent.getExtra("com.parse.Data",
							function(d) {
								$.mobile.navigate('#message');
								$.mobile.loading("show");
								app.message.received_push(JSON.parse(d))
							},
							function() {
								console.log('nothing received!')
							}
						)
					}
				},
				function () {

				}
			)
		}
	},

	on_mobile: function () {

		if (typeof device !== 'undefined') {
			return true
		} else {
			return false;
		}

	},

	custom_alert: function (error_object, message) {
		if (app.on_mobile() === true) {
			navigator.notification.alert(
				message,
				null,
				app.name,
				'Ok'
			);
			$.mobile.loading("hide");
		} else {
			alert(message)
			console.log(JSON.stringify('Error!', error_object));
		}
	},

	notification: function (message,callback) {
		if (app.on_mobile() === true) {
			navigator.notification.alert(
				message,  // message
				callback,         // callback
				app.name,            // title
				'ok'                  // buttonName
			);
		} else {
			alert(message)
		}
	}
};

var ua = navigator.userAgent.toLowerCase();
var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
if(isAndroid) {

    var deviceReadyDeferred = $.Deferred();
    var jqmReadyDeferred = $.Deferred();
	var parseReady = $.Deferred();
    document.addEventListener("deviceReady", deviceReady, false);

    function deviceReady() {
        deviceReadyDeferred.resolve();
	    window.parsePlugin.initialize(app.parse_app_id, app.parse_client_key, function () {
		     parseReady.resolve();
	    });
     }

    $(document).one("mobileinit", function () {
        jqmReadyDeferred.resolve();
    });

    $.when(deviceReadyDeferred, jqmReadyDeferred, parseReady).then(allReady);

} else {
    var jqmReadyDeferred = $.Deferred();


    $(document).one("mobileinit", function () {
        jqmReadyDeferred.resolve();
    });

    $.when(jqmReadyDeferred).then(allReady);
}

function allReady() {

    $('.opacity-wrapper').addClass('loaded check');

    $.extend(true, app, {
        home: {
	        init: function () {

	        }
        }
    });

    app.globals();

	try{
		app.home.init();
	} catch (e) {
		console.log('home trouble', e)
	}
	try{
		app.authentication.init();
	} catch (e) {
		console.log('auth trouble', e)
	}
	try{
		app.feed.init();
	} catch (e) {
		console.log('feed trouble', e)
	}
	try{
		app.addjob.init();
	} catch (e) {
		console.log('addjob trouble', e)
	}

	try{
		app.messagelist.init();
	} catch (e) {
		console.log('messagelist trouble', e)
	}

	try{
		app.myjobs.init();
	} catch (e) {
		console.log('jobs trouble', e)
	}


	try{
		app.message.init();
	} catch (e) {
		console.log('message trouble', e)
	}

	if ((app.authentication.is_logged_in() === true) && ((location.hash === '#home') || (location.hash === ''))) {
		$.mobile.navigate('#feed');
	}

}


