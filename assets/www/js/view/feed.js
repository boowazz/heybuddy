/*jslint node: true */
/*global app, google, $, jQuery, Handlebars, InfoBox, List, Cookies, _, Spinner, Pin, Stripe*/
"use strict";

jQuery.extend(true, app, {
    feed: {
        init: function () {

            var _self = this;

	        $(document).on("pagecreate", "#feed", function (e, ui) {
		        _self.listeners();
		        _self.job_object = Parse.Object.extend("Job");
	        });

            $(document).on("pagebeforeshow", '#feed', function () {
                if (app.authentication.is_logged_in() === false) {
                    $.mobile.navigate('#home');
                } else {
                    _self.refresh_feed();
                }
            });
            $(document).on("pageshow", "#feed", function (e, ui) {
                console.log('pageshow: #feed: This should fire when the feed is shown. ');
            });
        },

        listeners: function () {
            var _self = this;

	        $('.job-list').on('click', '.btn-offer-help', function (e) {
		        e.preventDefault();
		        $.mobile.loading("show");
		        app.message.load_thread($(this).closest('li').data('object-id'));
	        });

            $('.filter-button').click(function() {

                $.mobile.loading("show");

                //the + symbol changes the value into a number
                var _val = $(this).attr('data-value')

                console.log(_val);

                if ((_val === "1") || (_val === "3") || (_val === "5")) {
                    navigator.geolocation.getCurrentPosition(
                        function(location) {
                            _val = parseFloat(_val);
                            _self.change_distance(_val, location.coords.latitude, location.coords.longitude);
                        }
                    );
                } else if (_val === 'all') {
                    _self.refresh_feed();
                }
            });
        },

        refresh_feed: function () {

            var _self = this;

	        $.mobile.loading("show");

            var query = new Parse.Query(_self.job_object);
	        var current_user = Parse.User.current();
			query.include("user");

            query
	            .limit(10)
	            .descending("createdAt")
	            .notEqualTo("user", current_user)
	            .notEqualTo("status", "complete")
	            .find({
                success: function (results) {
	                var jsonArray = [];

	                for(var i = 0; i < results.length; i++) {
		                var result = results[i]
		                var resultJSON = results[i].toJSON();
		                var user  = result.get('user').toJSON();

		                resultJSON.name = user.name;

						//make sure the current user's jobs aren't listed
	                    jsonArray.push(resultJSON);
	                }


	                var context = {
		                jobs: jsonArray
	                };

	                var source = $('#job-template').html();
	                var template = Handlebars.compile(source);
	                var html = template(context);  // here's example with some details [link](http://screencast.com/t/XvPFuafRuIW)

                    $("#feed div[data-role=feed-content] .job-list").html(html);

	                $.mobile.loading("hide");

                },
                //if Something goes wrong
                error: function (e, error) {
	                app.custom_alert(e, 'Something went wrong! Please go to a different page and come back to see the feed.')
                }
            });
        },

        change_distance: function (distance, lat, lng) {

            var _self = this;

            console.log('distance, lat, lng', distance,lat,lng);

	        $.mobile.loading("show");

	        //Parse query to get results

	        var query = new Parse.Query(_self.job_object);
	        var current_user = Parse.User.current();
	        query.include("user");
            var point = new Parse.GeoPoint(lat, lng);

            query.limit(10)
	            .withinKilometers("jobCoord", point, distance)
	            .notEqualTo("user", current_user)
	            .notEqualTo("status", "complete")
	            .find({
	            success: function(results) {

	                var jsonArray = [];

		            if (results.length < 1) {
			            alert('Sorry, it looks like there are no jobs for that selection');
			            $("#feed div[data-role=feed-content] .job-list").html('');
		            } else {
			            for(var i = 0; i < results.length; i++) {
				            var result = results[i]
				            var resultJSON = results[i].toJSON();
				            var user  = result.get('user').toJSON();

				            resultJSON.name = user.name;

				            //make sure the current user's jobs aren't listed
				            jsonArray.push(resultJSON);
			            }

			            var context = {
				            jobs: jsonArray
			            };

			            var source = $('#job-template').html();
			            var template = Handlebars.compile(source);
			            var html = template(context);  // here's example with some details [link](http://screencast.com/t/XvPFuafRuIW)

			            $("#feed div[data-role=feed-content] .job-list").html(html);

			            $.mobile.loading("hide");

		            }


                },
                //if Something goes wrong
                error: function(e, error) {
	                app.custom_alert(e, 'Something went wrong! Please go to a different page and come back to see the feed.')
                }
            });
        }
    }
});

