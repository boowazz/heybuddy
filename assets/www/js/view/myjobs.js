jQuery.extend(true, app, {
	myjobs: {
		init: function () {
			var _self = this;
			console.log('Load my jobs list');

			$(document).on("pagecreate", "#myjobs", function (e, ui) {
				_self.job_object = Parse.Object.extend("Job");

			});

			$(document).on("pagebeforeshow", '#myjobs', function () {
				if (app.authentication.is_logged_in() === false) {
					$.mobile.navigate('#home');
				}
			});
			$(document).on("pageshow", '#myjobs', function () {
/*				$.mobile.loading( "show", {
					text: "Retrieving jobs"
				});*/
				_self.refresh_feed();
			});

		},

		disable_listeners: function () {
			$('.job-item-manage').off('click', '.cancel-job', function (e) {});
			$('.job-item-manage').off('click', '.complete-job', function (e) {});
		},

		listeners: function () {
			var _self = this;

			$('.job-item-manage').on('click', '.cancel-job', function (e) {
				e.preventDefault();
				var objectId = $(this).closest('.job-item-manage').attr('data-object-id')

				if (app.on_mobile() === true) {

					navigator.notification.confirm(
						'Are you sure you want to cancel?',  // message
						onConfirm,              // callback to invoke with index of button pressed
						'HeyBuddy',            // title
						['Ok','Cancel']      // buttonLabels
					);

				} else {
					onConfirm(1)
				}

				function onConfirm(buttonIndex) {

					var job = Parse.Object.extend("Job");
					var query = new Parse.Query(job);

					if (buttonIndex === 1) {

						query.get(objectId, {
							success: function(myObj) {
								// The object was retrieved successfully.
								myObj.destroy({});
								console.log('job removed');
								_self.refresh_feed();
							},
							error: function(object, error) {
								// The object was not retrieved successfully.
								// error is a Parse.Error with an error code and description.

							}
						});
					}

				}
			});

			$('.job-item-manage').on('click', '.complete-job', function (e) {
				e.preventDefault();

				var objectId = $(this).closest('.job-item-manage').attr('data-object-id')

				if (app.on_mobile() === true ) {
					navigator.notification.confirm(
						'Are you sure you want to complete job?',  // message
						onConfirm,              // callback to invoke with index of button pressed
						'HeyBuddy',            // title
						['Ok','Cancel']          // buttonLabels
					);
				} else {
					onConfirm(1)
				}

				function onConfirm(buttonIndex) {
					var job = Parse.Object.extend("Job");
					var query = new Parse.Query(job);

					if (buttonIndex === 1) {

						query.get(objectId, {
							success: function(jobAgain) {
								jobAgain.set("status", "complete");
								jobAgain.save(null, {
									success: function() {
										console.log('completed hhe')
										_self.refresh_feed()
									},
									error: function(userAgain, error) {
										// This will error, since the Parse.User is not authenticated
										console.log(error)
									}
								});
							},
							error: function(object, error) {
								// The object was not retrieved successfully.
								// error is a Parse.Error with an error code and description.

							}
						});
					}

				}

			});


		},


		refresh_feed: function () {
			var _self = this;
			var query = new Parse.Query(_self.job_object);
			var current_user = Parse.User.current();
			query.include("user");

			_self.disable_listeners();

			$.mobile.loading("show");

			//Gotta setup simultaneous queries omg

			var activeDeferred = $.Deferred();
			var completeDeferred = $.Deferred();



			query.equalTo("user", current_user).notEqualTo("status", "complete").find({
				success: function (results) {
					var jsonArray = [];

					for(var i = 0; i < results.length; i++) {
						var result = results[i];
						jsonArray.push(results[i].toJSON());
					}

					activeDeferred.resolve(jsonArray)

				},
				//if Something goes wrong
				error: function (e, error) {
					$.mobile.loading("hide");
					console.log(e);
				}
			});

			query.equalTo("user", current_user).equalTo("status", "complete").find({
				success: function (results) {
					var jsonArray = [];

					for(var i = 0; i < results.length; i++) {
						var result = results[i];
						jsonArray.push(results[i].toJSON());
					}
					completeDeferred.resolve(jsonArray)

				},
				//if Something goes wrong
				error: function (e, error) {
					$.mobile.loading("hide");
					console.log(e);
				}
			});


			$.when(activeDeferred, completeDeferred).then(function (context_active,context_complete) {
				_self.layout(context_active,context_complete)
			});

		},

		layout: function (context_active, context_complete) {

			var context = {
				active: context_active,
				complete: context_complete
			};

			var _self = this;

			var source = $('#jobs-page').html();
			var template = Handlebars.compile(source);
			var html = template(context);  // here's example with some details [link](http://screencast.com/t/XvPFuafRuIW)

			$("#myJobsFeed").html(html);

			_self.listeners();

			$.mobile.loading("hide");
		}

	}
});

