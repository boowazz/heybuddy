jQuery.extend(true, app, {
    addjob: {
        init: function () {
            var _self = this;

            $(document).on("pagecreate", '#addJob', function () {
                console.log('pagecreate: #addJob: This should fire only once.');
                if (app.authentication.is_logged_in() === false) {
                    $.mobile.navigate('#home');
                }
                _self.listeners();
	            _self.job_object = Parse.Object.extend("Job");
            });
            $(document).on("pagebeforeshow", '#addJob', function () {
                if (app.authentication.is_logged_in() === false) {
                    $.mobile.navigate('#home');
                }
            });

        },
        listeners: function () {

            var _self = this;

            $('.js-change-date').change(function () {
                if ($(this).val() !== '') {
                    if ($(this).find('option:first').val() === '') {
                        $(this).find('option:first').remove();
                    }
                }
            });

            $('.js-radio-input-time').change(function () {
                console.log($(this).val())
                var selectedValue = $(this).val();
                if (selectedValue === "Later") {
                    $('.select-date').removeClass('hidden')
                } else {
                    $('.select-date').addClass('hidden')
                }
            });

            $("#takePicBtn").on("click", function (e) {
                e.preventDefault();
                navigator.camera.getPicture(_self.photo_success, _self.photo_failure, {
                    destinationType: Camera.DestinationType.FILE_URI,
                    sourceType: Camera.PictureSourceType.CAMERA,
                    encodingType: Camera.EncodingType.JPEG,
                    targetWidth: 800,
                    targetHeight: 800,
                    quality: 50,
                    saveToPhotoAlbum: false,
                    correctOrientation: true
                });
                $.mobile.loading("show");
            });

            // ************************************
            //  When you click the Save Job button
            // ************************************

            $("#saveJobBtn").on("click", function (e) {

	            e.preventDefault();

                var errors = "",
                    imagedata = $('#takePicBtn').attr('data-image-url'),
                    jobPicFile,
                    jobText = $("#jobText").val(),
	                jobDate,
	                jobLocationObject;

	            //check date
                if ($('input[name="now_later"]:checked').val() === 'Later') {
                    if (_self.valid_date() === true) {
	                    jobDate = _self.custom_job_date();
                        console.log('the custom date is: ', jobDate.format('DD/MM'))
                    } else {
                        //milly-to-read: you can replace this with the notification.alert popup from the phonegap docs:
                        //http://docs.phonegap.com/en/2.9.0/cordova_notification_notification.md.html#notification.alert
                        errors += 'Please select how many days from now you require help.\n'
                    }
                } else {
	                jobDate = new moment();
                }

				//check title
                if (jobText === '') {
                    errors += 'Please write a job title. \n'
                }

                if (errors !== '') {
	                if (app.on_mobile() === true) {
		                alert(errors);
	                } else {

	                }
                    return;
                }

	            $.mobile.loading('show');

	            var geo_point_creation = $.Deferred();

	            _self.get_coordinates(function (lat,lng,suburb) {
		            var locationObject = {
			            latitude:lat,
			            longitude:lng,
			            suburb:suburb
		            }

		            geo_point_creation.resolve(locationObject)

	            })



                if (typeof imagedata !== "undefined" && app.on_mobile() === true) {

	                var image_creation = $.Deferred();

	                _self.create_image(jobText, imagedata, function (jobPhoto) {
		                //_self.create_job(jobDate, jobText, jobPhoto)

		                console.log('image being created, is it working?')
		                var thing = jobPhoto.url();

		                image_creation.resolve(jobPhoto);

	                });

	                $.when(geo_point_creation, image_creation).then(function (locationObject, imageObject) {
		                console.log('ok were going into the creation of the job');
		                _self.create_job(jobText, jobDate, locationObject, imageObject)
	                });

                } else {
	                $.when(geo_point_creation).then(function (location) {
		                _self.create_job(jobText, jobDate, location)
	                });
                }
            });

        },



	    create_image: function (jobText, imagedata, callback) {
		    var _self = this;
		    window.imageResizer.resizeImage(
			    function (data) {
				    save_image(data.imageData);
			    },
			    function (data) {
				    //fail
				    console.log('Image resizing failure!', data)
			    },
			    imagedata,
			    800,
			    800,
			    {
				    format: 'jpg',
				    imageDataType: 'urlImage',
				    resizeType: 'pixelResize'
			    }
		    );

		    function save_image(base64) {

			    var jobPicFile = new Parse.File(_self.sanitize_file_name(jobText) + '-jobPic.jpg', {base64: base64});

			    jobPicFile.save().then(
				    function () {
					    console.log('job pick file saved yayayayya what a nice time.');
				        callback(jobPicFile)
		            },
				    function (error) {
					    console.log("File object save problem.");
					    console.log(JSON.stringify(error));
					    $.mobile.loading("hide");
				    }
			    );
		    }

	    },

	    create_job: function (text, date, locationObject, imageObject) {

	       var _self = this,
		        job = new _self.job_object,
		       user = Parse.User.current();

		    $.mobile.loading("show");

		    job.set("jobTitle", text);
		    job.set("user", user);
		    job.set("jobDate", date.toDate());

		    console.log(date.toDate());

		    job.set("jobCoord", new Parse.GeoPoint({
			    latitude: locationObject.latitude,
			    longitude: locationObject.longitude
		    }));

		    job.set("jobLocation", locationObject.suburb);

		    if (typeof imageObject !== 'undefined') {
			    job.set("jobPic", imageObject);
		    }

		    console.log('off to send job!')

		    _self.send_job(job)

	    },

	    send_job: function (job) {
			var _self =  this;
		    job.save(null, {
			    success: function (ob) {
				    $.mobile.navigate("#feed");
				    _self.cleanup();
			    },
			    error: function (e, error) {
				    if (app.on_mobile() === true) {
					    navigator.notification.confirm(
						    'Sorry, it looks like saving the job failed. Please try again.', // message
						    onConfirm,            // callback to invoke with index of button pressed
						    'Game Over',           // title
						    ['Try Again','Edit Job']         // buttonLabels
					    );
				    } else {
                        console.log(JSON.stringify(e));
					    alert('Job save failed. Please try again.')
				    }
			    }
		    });

		    function onConfirm (buttonIndex) {
			    $.mobile.loading("hide");
			    if (buttonIndex === 1) {
				    _self.send_job(job)
			    }
		    }

	    },

        custom_job_date: function () {
            var jobDate = moment(),
                plusDays = parseFloat($('#daySelect').val());
            jobDate.add('days', plusDays);
            return jobDate;
        },

        valid_date: function () {
            if ($("#daySelect").val() === '') {
                return false;
            } else {
                return true;
            }
        },

	    get_coordinates: function (callback) {
            navigator.geolocation.getCurrentPosition(
                function (location) {

                    var lat = location.coords.latitude,
                        lng = location.coords.longitude,
                        geocoder = new google.maps.Geocoder(),
                        latlng = new google.maps.LatLng(lat, lng);

                    geocoder.geocode({
                        'latLng': latlng
                    }, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[1]) {
                                var jobLocation = JSON.stringify(results[1].address_components[0].short_name);
	                            callback(lat, lng, jobLocation);
	                            //results[1].get('suburb')

                            } else {
                                console.log('No results found');
                            }
                        } else {
	                        console.log('Geocoder failed due to: ' + status);
	                        app.custom_alert(status, 'We couldn\'t find your location.')

                        }

                    });
                },
                function (error) {
                    //error!
	                app.custom_alert(error, 'We couldn\'t find your location.')
                }
            );
        },

        photo_success: function (data) {
            // start loader here for pic
            $.mobile.loading("hide");
            console.log('got here');
            $("#takePicBtn").addClass('picture-taken').attr('data-image-url', data);
            console.log("File URI Comes from Cordova: " + data);
        },

        photo_failure: function (data) {

            if (app.on_mobile() === true) {
                navigator.notification.alert(
	                'We weren\'t able to capture that. Please try again.',
	                null,
	                [app.name],
	                ['Ok']
                );
            } else {
                alert('We weren\'t able to capture that. Please try again.')
            }

            $("#takePicBtn").addClass(".picture-error")
            console.log("error grabbing camera/gallery", e);
        },

        cleanup: function () {
	        var _self = this;
            $("#jobText").val("");
	        $("#takePicBtn").removeAttr('data-image-url');

	        _self.job_object = new _self.job_object;

        },

	    sanitize_file_name: function (str) {

		    str = str.replace(/^\s+|\s+$/g, ''); // trim
		    str = str.toLowerCase();

		    // remove accents, swap ñ for n, etc
		    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
		    var to   = "aaaaeeeeiiiioooouuuunc------";
		    for (var i=0, l=from.length ; i<l ; i++) {
			    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
		    }

		    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
			    .replace(/\s+/g, '-') // collapse whitespace and replace by -
			    .replace(/-+/g, '-'); // collapse dashes

		    return str;

	    }
    }
});



