jQuery.extend(true, app, {
    authentication: {
        init: function () {
            var _self = this;

            if (app.on_mobile() === true) {
                console.log('Auth init on mobile')
	            FB.init({
                    appId: app.fb_app_id,
                    nativeInterface: CDV.FB,
                    useCachedDialogs: false
                });

            } else {
                window.fbAsyncInit = function() {
                    FB.init({
                        appId      : app.fb_app_id,
                        status     : true,
                        xfbml      : true
                    });
                };
                (function(d, s, id){
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) {return;}
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/en_US/all.js";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));
            }

	        $.mobile.loading("hide");

            _self.login_button();
        },

        is_logged_in: function () {

	        var currentUser = Parse.User.current();

	        if (currentUser) {
		        return true;
	        } else {
		       return false
	        }

        },

        login_button: function () {
            var _self = this;
            $('.js-login').on('click', function (e) {
                e.preventDefault();
                _self.fb_login();
            });
        },

        fb_login: function () {

            var _self = this;

            $.mobile.loading("show");

            FB.login(function (response) {
                if (response.authResponse) {
                    if (app.on_mobile() === true) {
                        var validDate = new moment(response.authResponse.expirationTime);
                        var fbData = {
                            'id': response.authResponse.userId + "",
                            'access_token': response.authResponse.accessToken,
                            'expiration_date': validDate.toISOString()
                        }

                        _self.parse_login(fbData);

                    } else {
                        console.log(response)

                        var fbData = {
                            'id': response.authResponse.userID + "",
                            'access_token': response.authResponse.accessToken,
                            'expiration_date': new moment('12-20-2014').toISOString()
                        }

                        console.log('On Desktop FB Data', fbData);

                        _self.parse_login(fbData);
                    }

                } else {
	                navigator.notification.alert(
		                'Something went wrong. Please try logging in again.',
		                null,
		                app.name,
		                'Ok'
	                );
	                $.mobile.loading("hide");
                }

            }, {scope: 'email'});

        },

        parse_login: function (fbData) {
            var _self = this;
            Parse.FacebookUtils.logIn(fbData, {
                success: function (_user) {
	                console.log(_user)
	                _self.update_parse_user(_user, function () {
		                _self.update_subscriptions(_user);
	                });
	            },
                error: function (error1, error2) {
	                $.mobile.loading("hide");
                    console.log("Unable to create/login to as Facebook user");
                    alert('Sorry, it looks like we couldn\'t log you in');
                    console.log("  ERROR1 = " + JSON.stringify(error1));
                    console.log("  ERROR2 = " + JSON.stringify(error2));
                }
            });
        },

	    update_parse_user: function (_user, callback) {

		    var ID = _user.attributes.authData.facebook.id,
			    user = Parse.User.current();

		    var userDeferred = $.Deferred();
		    var picDeferred = $.Deferred();

			var userDetails = FB.api("/"+ID, function (response) {
				userDeferred.resolve(response)
			});

		    var picDetails = FB.api("/"+ID+"/picture", function (response) {
			    picDeferred.resolve(response);
		    });

		    $.when(userDeferred, picDeferred).then(function(userDetails,picDetails) {
			    user.set('fbId',userDetails.id);
			    user.set('name', userDetails.name);
			    user.set('firstname', userDetails.first_name);
			    user.set('picture', picDetails.data.url);
			    user.save(null,{
				    success: function(test) {
					    callback()
				    },
				    error: function (e) {
					    navigator.notification.alert(
						    'Something went wrong. Please try logging in again.',
						    null,
						    app.name,
						    'Ok'
					    );
					    $.mobile.loading("hide");
				    }
			    })
		    });
	    },

	    update_subscriptions: function (_user) {
		    var _self = this;

		    if (app.on_mobile() === true) {
			    window.parsePlugin.getSubscriptions(function(subscriptions) {
					console.log(subscriptions.length)
				    if (subscriptions.length > 0) {
					    _self.unsubscribe(_user);
				    } else {
					    _self.subscribe(_user);
				    }
			    }, function(e) {
				    navigator.notification.alert(
					    'Something went wrong. Please try logging in again.',
					    null,
					    app.name,
					    'Ok'
				    );
				    $.mobile.loading("hide");
			    });
		    } else {
			    _self.subscribe(_user);
		    }

	    },

	    unsubscribe: function (_user) {
		    var _self = this;
		    //does this actually work?
		    console.log('unsubscribe')
		    if (app.on_mobile() === true) {
			    window.parsePlugin.unsubscribe(
				    "custom_"+_user.id,
				    function() {
				        _self.subscribe(_user)
				    },
				    function () {
					    _self.subscribe(_user)
		            }
			    );
		    } else {
			    _self.subscribe(_user)
		    }
	    },

	    subscribe: function (_user) {
		    var _self = this;
		    console.log('subscribe')
		    if (app.on_mobile() === true) {
			    window.parsePlugin.subscribe(
				    "custom_"+_user.id,
				    function(id) {
				        _self.login();
			        },
				    function(id) {
					    navigator.notification.alert(
						    'Something went wrong. Please try logging in again.',
						    null,
						    app.name,
						    'Ok'
					    );
					    console.log('!!subscribe error!!')
					    $.mobile.loading("hide");

				    }
			    );
		    } else {
			    _self.login();
		    }
	    },

	    test_push: function (id) {

		    var _self = this;
		    var channel = "custom_"+ id;

		    if (app.on_mobile() === true) {

			    Parse.Push.send({
				    channels: [ channel ],
				    data: {
					    alert: "The user ID of me is: " + (Math.random() * (10 - 2) + 2),
					    'jobID': 'eggs',
					    'userID': 'booooo',
					    'phonoSessionID': 3209480948
				    }
			    }, {
				    success: function(test) {
					    // Push was successful
					    console.log(test);
					    _self.login();
				    },
				    error: function(error) {
					    // Handle error
					    navigator.notification.alert(
						    'Something went wrong. Please try logging in again.',
						    null,
						    app.name,
						    'Ok'
					    );
					    console.log('!!Push test error!!')
					    $.mobile.loading("hide");
				    }
			    });

		    } else {
			    _self.login();
		    }
	    },


        login: function () {
            $.mobile.loading("hide");
            $.mobile.navigate("#feed");
        },

        logout: function () {
			var current_user = Parse.User.current();

	        if (app.on_mobile() === true) {

		        window.parsePlugin.unsubscribe(
			        "custom_"+current_user.id,

			        function() {
				        console.log('logout function in')
				        Parse.User.logOut();
				        $.mobile.navigate("#home");
		            },

			        function (e) {
				        console.log(e)
				        Parse.User.logOut();
				        $.mobile.navigate("#home");
			        }

		        );
	        } else {
		        Parse.User.logOut();
		        $.mobile.navigate("#home");
	        }


        }
    }
});