jQuery.extend(true, app, {
	messagelist: {
		init: function () {
			console.log('Message list loaded');

			var _self = this;

			$(document).on("pagecreate", '#messagelist', function () {
				console.log('pagecreate: #messagelist: This should fire only once.');
				if (app.authentication.is_logged_in() === false) {
					$.mobile.navigate('#home');
				} else {
					_self.listeners();
					console.log('listeners')
				}

			});

			$(document).on("pagebeforeshow", '#messagelist', function () {
				if (app.authentication.is_logged_in() === false) {
					$.mobile.navigate('#home');
				} else {
					//now we refresh the list plz ok!
					_self.refresh_list();
				}
			});

		},
		listeners: function () {
			console.log($('.js-launch-message'));
			$('.chat-list').on('click', '.msg-item', function (e) {
				e.preventDefault();
				$.mobile.loading("show");
				app.message.load_thread($(this).data('object-id'));
			});
		},
		refresh_list: function() {

			console.log('why fire twice?')


			$.mobile.loading( "show", {
				text: "Starting chat"
			});
			var results = [],
				template_messages = [];

			for (var i = 0; i < window.localStorage.length; i++) {
				var key = window.localStorage.key(i);
				if (key.slice(0,5) === "chat_") {
					results.push(JSON.parse(window.localStorage.getItem(key)));
				}
			}

			if (results.length > 0) {
				for (var i = 0; i < results.length; i++) {

					console.log(results[i].messages);
					console.log(results[i].messages.length);

					if (results[i].messages.length > 0) {
						var lastThread = results[i].messages[(results[i].messages.length)-1]

						var message = {
							name: results[i].recipient.name,
							message: lastThread.content,
							timestamp: lastThread.timestamp,
							objectId: results[i].objectId
						};

						template_messages.push(message)
					}

				}

			}
			var context = {
				threads: template_messages
			};
			var source = $('#messagelist-template').html();
			var template = Handlebars.compile(source);
			console.log(context);
			var html = template(context);
			$("#messagelistContainer").html(html);
			this.listeners();
			$.mobile.loading("hide");

		}
	}
});

