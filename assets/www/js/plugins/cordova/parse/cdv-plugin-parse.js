/**
 * cordova Push Subscriptions
 *
 */

var parsePlugin = {
	initialize: function(appId, clientKey, successCallback, errorCallback) {
		cordova.exec(
			successCallback,
			errorCallback,
			'ParsePlugin',
			'initialize',
			[appId, clientKey]
		);
	},

	getInstallationId: function(successCallback, errorCallback) {
		cordova.exec(
			successCallback,
			errorCallback,
			'ParsePlugin',
			'getInstallationId',
			[]
		);
	},

	getInstallationObjectId: function(successCallback, errorCallback) {
		cordova.exec(
			successCallback,
			errorCallback,
			'ParsePlugin',
			'getInstallationObjectId',
			[]
		);
	},

	getSubscriptions: function(successCallback, errorCallback) {
		cordova.exec(
			successCallback,
			errorCallback,
			'ParsePlugin',
			'getSubscriptions',
			[]
		);
	},

	subscribe: function(channel, successCallback, errorCallback) {
		cordova.exec(
			successCallback,
			errorCallback,
			'ParsePlugin',
			'subscribe',
			[ channel ]
		);
	},

	unsubscribe: function(channel, successCallback, errorCallback) {
		cordova.exec(
			successCallback,
			errorCallback,
			'ParsePlugin',
			'unsubscribe',
			[ channel ]
		);
	}
};

/*

(function(cordova){
	var ParsePlugin = function() {

	};

	ParsePlugin.prototype.initialize = function(appId, clientKey, successCallback, errorCallback) {
		cordova.exec(function(args) {
			successCallback(args);
		}, function(args) {
			errorCallback(args);
		}, [appId, clientKey]);
	};

	ParsePlugin.prototype.getInstallationId = function(successCallback, errorCallback) {
		cordova.exec(function(args) {
				successCallback(args);
			}, function(args) {
				errorCallback(args);
			}, 'ParsePlugin',
			'getInstallationId',
			[]
		);
	};

	ParsePlugin.prototype.getInstallationObjectId = function(successCallback, errorCallback) {
		cordova.exec(function(args) {
				successCallback(args);
			}, function(args) {
				errorCallback(args);
			}, 'ParsePlugin',
			'getInstallationObjectId',
			[]
		);
	};

	ParsePlugin.prototype.getSubscriptions = function(successCallback, errorCallback) {
		cordova.exec(function(args) {
				successCallback(args);
			}, function(args) {
				errorCallback(args);
			}, 'ParsePlugin',
			'getSubscriptions',
			[]
		);
	};

	ParsePlugin.prototype.subscribe = function(channel, successCallback, errorCallback) {
		cordova.exec(function(args) {
				successCallback(args);
			}, function(args) {
				errorCallback(args);
			}, 'ParsePlugin',
			'subscribe',
			[ channel ]
		);
	};

	ParsePlugin.prototype.unsubscribe =function(channel, successCallback, errorCallback) {
		cordova.exec(function(args) {
				successCallback(args);
			}, function(args) {
				errorCallback(args);
			}, 'ParsePlugin',
			'unsubscribe',
			[ channel ]
		);
	};
	cordova.addConstructor(function() {
		window.parsePlugin = new ParsePlugin();

		// backwards compatibility
		window.plugins = window.plugins || {};
		window.plugins.parsePlugin = window.parsePlugin;
	});
})(window.PhoneGap || window.Cordova || window.cordova);*/
