/*jslint node: true */
/*global app, google, $, jQuery, Handlebars, InfoBox, List, Cookies, _, Spinner, Pin, Stripe*/
"use strict";

jQuery.extend(true, app, {
    feed: {
        init: function () {

            var _self = this;

	        $(document).on("pagecreate", "#feed", function (e, ui) {
		        _self.listeners();
		        _self.job_object = Parse.Object.extend("Job");
	        });

            $(document).on("pagebeforeshow", '#feed', function () {
                if (app.authentication.is_logged_in() === false) {
                    $.mobile.navigate('#home');
                } else {
                    _self.refresh_feed();
                }
            });
            $(document).on("pageshow", "#feed", function (e, ui) {
                console.log('pageshow: #feed: This should fire when the feed is shown. ');
            });
        },

        listeners: function () {
            var _self = this;
            $('.filter-button').click(function() {
                $.mobile.loading("show");

                //the + symbol changes the value into a number
                var _val = $(this).attr('data-value')

                console.log(_val);

                if ((_val === "1") || (_val === "3") || (_val === "5")) {
                    navigator.geolocation.getCurrentPosition(
                        function(location) {
                            _val = parseFloat(_val);
                            _self.change_distance(_val, location.coords.latitude, location.coords.longitude);
                        }
                    );
                } else if (_val === 'all') {
                    _self.refresh_feed();
                }
            });
        },

        refresh_feed: function () {

            var _self = this;

            var query = new Parse.Query(_self.job_object);

            query.limit(10).descending("createdAt").find({
                success: function (results) {
	                var jsonArray = [];

	                for(var i = 0; i < results.length; i++) {
		                jsonArray.push(results[i].toJSON());
	                }

	                var context = {
		                jobs: jsonArray
	                };

	                var source = $('#job-template').html();
	                var template = Handlebars.compile(source);
	                var html = template(context);  // here's example with some details [link](http://screencast.com/t/XvPFuafRuIW)

                    $("#feed div[data-role=feed-content] .job-list").html(html);
	                $( ".job-list" ).listview( "refresh" );
	                $.mobile.loading("hide");

                },
                //if Something goes wrong
                error: function (e, error) {
                    $.mobile.loading("hide");
                    console.log(e);
                }
            });
        },

        change_distance: function (distance, lat, lng) {

            var _self = this;

            console.log('distance, lat, lng', distance,lat,lng);

            $.mobile.loading("show");

            //Parse query to get results
            var query = new Parse.Query(_self.job_object);
            var point = new Parse.GeoPoint(lat, lng);

            query.limit(10).withinKilometers("jobCoord", point, distance).find({
                success: function(results) {

	                var jsonArray = [];

	                for(var i = 0; i < results.length; i++) {
		                jsonArray.push(results[i].toJSON());
	                }

	                var context = {
		                jobs: jsonArray
	                };

	                var source = $('#job-template').html();
	                var template = Handlebars.compile(source);
	                var html = template(context);  // here's example with some details [link](http://screencast.com/t/XvPFuafRuIW)

	                $("#feed div[data-role=feed-content] .job-list").html(html);
	                $( ".job-list" ).listview( "refresh" );

	                $.mobile.loading("hide");

                },
                //if Something goes wrong
                error: function(e, error) {
                    $.mobile.loading("hide");
                    console.log(e);
                }
            });
        }
    }
});

