jQuery.extend(true, app, {
    authentication: {
        init: function () {
            var _self = this;

            if (app.on_mobile() === true) {
                console.log('Auth init on mobile')
	            FB.init({
                    appId: app.fb_app_id,
                    nativeInterface: CDV.FB,
                    useCachedDialogs: false
                });

            } else {
                window.fbAsyncInit = function() {
                    FB.init({
                        appId      : app.fb_app_id,
                        status     : true,
                        xfbml      : true
                    });
                };
                (function(d, s, id){
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) {return;}
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/en_US/all.js";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));
            }

	        $.mobile.loading("hide");

            _self.login_button();
        },

        is_logged_in: function () {

	        var currentUser = Parse.User.current();

	        if (currentUser) {
		        return true;
	        } else {
		       return false
	        }

        },

        login_button: function () {
            var _self = this;
            $('.js-login').on('click', function (e) {
                e.preventDefault();
                _self.fb_login();
            });
        },

        fb_login: function () {

            var _self = this;

            $.mobile.loading("show");

            FB.login(function (response) {
                if (response.authResponse) {

                    if (app.on_mobile() === true) {
                        var validDate = new moment(response.authResponse.expirationTime);
                        var fbData = {
                            'id': response.authResponse.userId + "",
                            'access_token': response.authResponse.accessToken,
                            'expiration_date': validDate.toISOString()
                        }

                        _self.parse_login(fbData);

                    } else {
                        console.log(response)

                        var fbData = {
                            'id': response.authResponse.userID + "",
                            'access_token': response.authResponse.accessToken,
                            'expiration_date': new moment('12-20-2014').toISOString()
                        }

                        console.log('On Desktop FB Data', fbData);

                        _self.parse_login(fbData);
                    }

                } else {
                    console.log('User cancelled login or did not fully authorize.');
                }

            }, {scope: 'email'});

        },

        parse_login: function (fbData) {
            var _self = this;
            Parse.FacebookUtils.logIn(fbData, {
                success: function (_user) {
	                _self.subscription(_user);
                },
                error: function (error1, error2) {
                    console.log("Unable to create/login to as Facebook user");
                    alert('Sorry, it looks like we couldn\'t log you in');
                    console.log("  ERROR1 = " + JSON.stringify(error1));
                    console.log("  ERROR2 = " + JSON.stringify(error2));
                }
            });
        },

	    subscription: function (_user) {
		    var _self = this;
		    parsePlugin.initialize(app.parse_app_id, app.parse_client_key, _self.update_subscriptions(_user));
	    },

	    update_subscriptions: function (_user) {
		    var _self = this;

		    if (app.on_mobile() === true) {
			    parsePlugin.getSubscriptions(function(subscriptions) {
					console.log(subscriptions)
				    if (subscriptions.length > 0) {
					    _self.unsubscribe(_user);
				    } else {
					    _self.subscribe(_user);
				    }
			    }, function(e) {
				    navigator.notification.alert(
					    'Something went wrong. Please try logging in again.',
					    null,
					    app.name,
					    'Ok'
				    );
				    console.log('!!update_subscriptions error!!')
			    });
		    } else {
			    _self.subscribe(_user);
		    }

	    },

	    unsubscribe: function (_user) {
		    var _self = this;
		    //does this actually work?
		    console.log('unsubscribe')
		    if (app.on_mobile() === true) {
			    parsePlugin.unsubscribe(
				    "custom_"+_user.id,
				    function() {
				        _self.subscribe(_user)
				    },
				    function () {
					    navigator.notification.alert(
						    'Something went wrong. Please try logging in again.',
						    null,
						    app.name,
						    'Ok'
					    );
					    console.log('!!unsubscribe error!!')
		            }
			    );
		    } else {
			    _self.subscribe(_user)
		    }
	    },

	    subscribe: function (_user) {
		    var _self = this;
		    console.log('subscribe')
		    if (app.on_mobile() === true) {
			    parsePlugin.subscribe(
				    "custom_"+_user.id,
				    function(id) {
				        _self.login();
			        },
				    function(id) {
					    navigator.notification.alert(
						    'Something went wrong. Please try logging in again.',
						    null,
						    app.name,
						    'Ok'
					    );
					    console.log('!!subscribe error!!')

				    }
			    );
		    } else {
			    _self.login();
		    }
	    },

	    test_push: function (id) {

		    var _self = this;
		    var channel = "custom_"+ id;

		    if (app.on_mobile() === true) {

			    Parse.Push.send({
				    channels: [ channel ],
				    data: {
					    alert: "The user ID of me is: " + (Math.random() * (10 - 2) + 2),
					    'jobID': 'eggs',
					    'userID': 'booooo',
					    'phonoSessionID': 3209480948
				    }
			    }, {
				    success: function(test) {
					    // Push was successful
					    console.log(test);
					    _self.login();
				    },
				    error: function(error) {
					    // Handle error
					    navigator.notification.alert(
						    'Something went wrong. Please try logging in again.',
						    null,
						    app.name,
						    'Ok'
					    );
					    console.log('!!Push test error!!')
				    }
			    });

		    } else {
			    _self.login();
		    }
	    },


        login: function () {
            $.mobile.loading("hide");
            $.mobile.navigate("#feed");
        },

        logout: function () {
			var current_user = Parse.User.current();

	        if (app.on_mobile() === true) {
		        parsePlugin.unsubscribe("custom_"+current_user.id, function() {
			        Parse.User.logOut();
			        $.mobile.navigate("#home");
		        });
	        } else {
		        Parse.User.logOut();
		        $.mobile.navigate("#home");
	        }


        }
    }
});