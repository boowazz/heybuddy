/*
 * milly-to-read
 * This is the official cordova plain javascript listener. If you're doing testing + cordova, uncomment the first two
 * lines below and then the closing bracket at the end of the JS file.
 *
 * */

/*document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {*/

/*if ((typeof cordova == 'undefined') && (typeof Cordova == 'undefined')) alert('Cordova variable does not exist. Check that you have included cordova.js correctly');
    if (typeof CDV == 'undefined') alert('CDV variable does not exist. Check that you have included cdv-plugin-fb-connect.js correctly');
    if (typeof FB == 'undefined') alert('FB variable does not exist. Check that you have included the Facebook JS SDK file.');
*/
//PARSE APP AND JS KEYS
var parseAPPID = "o6UmKN6Q81i9tHglqCtWVWMbz437ssAzNLrWuvwI",
    parseJSID = "0HMBdVJgrpYOc0yAQDdUXFQqMtLMB3DPOAhMEOND";

//Initialize Parse
Parse.initialize(parseAPPID, parseJSID);
//var Ismage = require("parse-image");
//Global Variables
var JobOb = Parse.Object.extend("Job"),
    imagedata = "",
    lat = 0,
    lng = 0,
    jobLocation = "",
    geocoder,
    saveMoment = "",
    pageShowCounter = 0,
    distanceCalc = 0,
    newDateFromNow = moment(),
    daysFrom = 0;


/* FB.init({
        appId: '717765138254866',
        nativeInterface: CDV.FB,
        useCachedDialogs: false
    });*/

$(function() {
    $('.js-login-please').click(function(e) {

        e.preventDefault();

        //FB.login(null, {scope: 'email'});
        FB.login(function(response) {
            if (response.authResponse) {
                var validDate = new moment(response.authResponse.expirationTime);
                var fbData = {
                    'id': response.authResponse.userId + "",
                    'access_token': response.authResponse.accessToken,
                    'expiration_date': validDate.toISOString()
                }
                parseLogin(fbData);

            } else {
                console.log('User cancelled login or did not fully authorize.');
            }
        }, {
            scope: 'email'
        });


        function parseLogin(fbData) {
            console.log(fbData);
            Parse.FacebookUtils.logIn(fbData, {
                success: function(_user) {
                    console.log("User is logged into Parse");
                    $.mobile.navigate("#addJob");

                },

                error: function(error1, error2) {
                    console.log("Unable to create/login to as Facebook user");
                    console.log("  ERROR1 = " + JSON.stringify(error1));
                    console.log("  ERROR2 = " + JSON.stringify(error2));
                }
            });
        }

    });

});

/************** PHONO CODE *********/

/*    $(document).on("mobileinit", function() {

        console.log('mobileinit: This should first');

        var audioType;

        if (navigator.javaEnabled()) {
            console.log('java enabled');
            audioType = 'java';
        } else {
            audioType = 'none';
        }

        $.phono({
            apiKey: "bc1cdeaaab263d8ee2ec6eb6c26bdf1d",
            audio: {
                type: 'none'
            },
            onReady: function(event) {
                //alert("My XMPP address is " + this.sessionId);
                console.log("My XMPP address is " + this.sessionId);
                //alert("My XMPP address is " + this.sessionId);
            },
            messaging: {
                onMessage: function(event) {
                    var message = event.message;
                    console.log("Message from: " + message.from + " " + message.body);
                    //alert("Message from: " + message.from + "\n" + message.body);
                }
            }
        });

        $('.nav-messages').click(function() {
            //append a date string to the end so that it forces a reload properly
            window.location = 'index.html?' + moment().unix();
        });
    });*/

/**********************************************************/

//When the #feed page is shown
$(document).on("pageshow", "#feed", function(e, ui) {

    pageShowCounter++;
    console.log('pageshow: #feed: This should fire when the feed is shown. It has been shown: ' + pageShowCounter + ' times');

    $.mobile.loading("show");

    //Parse query to get results
    var query = new Parse.Query(JobOb);

    query.limit(10).descending("createdAt").find({
        success: function(results) {
            $.mobile.loading("hide");

            var s = "";
            var shortDate = moment();
            for (var i = 0; i < results.length; i++) {
                //Inputting the HTML into #feed div[data-role=feed-content]
                // Including the job title, job Date, Picture and offer help button
                s += "<div class='job-details'>";

                //Job title
                s += "<p class='job-text'>" + "'" + results[i].get("jobTitle") + "'" + "</p>";

                //Job Picture
                var pic = results[i].get("jobPic");
                if (pic) {
                    s += "<div class='jobPicture'>" + "<img src='" + pic.url() + "'>" + "</div>";
                }

                //Job Date
                shortDate = moment(results[i].get("jobDate"));
                s += "<h6 class='jobTimeText'>" + shortDate.format("DD/MM") + "</h6>";


                s += "<h6 class='jobLocationText'>" + results[i].get("jobLocation") + "</h6>";
                //End DIV
                s += "</div>";

                //console.log('shortDate is: ' + shortDate.format("DD/MM"));

            }
            $("#feed div[data-role=feed-content]").html(s);

        },
        //if Something goes wrong
        error: function(e, error) {
            $.mobile.loading("hide");
            console.log(e);
        }
    });
});

//On the #addJob Page, for inputting jobs
$(document).on("pagecreate", '#addJob', function() {

    console.log('pagecreate: #addJob: This should fire only once.');

    $("#nowBtn").on("click", function(e) {
        var jobDate = moment();
        console.log('you clicked nowBtn and Date is ' + jobDate.format("DD/MM/YYYY"));
    });

    $('.js-change-date').change(function() {
        if ($(this).val() !== '') {
            if ($(this).find('option:first').val() === '') {
                $(this).find('option:first').remove();
            }
        }
    });

    $('.js-radio-input-time').change(function() {
        console.log($(this).val())
        var selectedValue = $(this).val();
        if (selectedValue === "Later") {
            $('.select-date').removeClass('hidden')
        } else {
            $('.select-date').addClass('hidden')
        }
    });

    // ************************************
    //  GET DAYS FROM NOW CODE - Milly
    // ************************************

    function getCustomJobDate() {
        daysFrom = $('#daySelect').val();
        newDateFromNow = moment().add('days', daysFrom);
        console.log(daysFrom, newDateFromNow.format('DD,MM'));
        return newDateFromNow;
    }

    // ************************************
    //  END
    // ************************************

    function isValidDate() {
        if ($("#daySelect").val() === '') {
            return false;
        } else {
            return true;
        }
    }


    // ************************************
    //  When you click the Save Job button
    // ************************************

    $("#saveJobBtn").on("click", function(e) {

        var jobDate = moment(),
            errors = '';

        console.log('Clicked the save job button.');

        e.preventDefault();

        //$(this).attr("disabled", "disabled").button("refresh");

        var jobText = $("#jobText").val();

        if ($('input[name="now_later"]:checked').val() === 'Later') {
            if (isValidDate() === true) {
                console.log('Save button: Custom date selected!');
                jobDate = getCustomJobDate();
                console.log('the custom date is: ', jobDate.format('DD/MM'))
            } else {

                //milly-to-read: you can replace this with the notification.alert popup from the phonegap docs:
                //http://docs.phonegap.com/en/2.9.0/cordova_notification_notification.md.html#notification.alert
                errors += 'Please select both a day and month.\n'
            }
        }

        if (jobText === '') {
            errors += 'Please write a job title. \n'
        }

        if (errors !== '') {
            alert(errors);
            return;
        }

        if (imagedata !== "") {

            var jobPicFile;

            console.log('when the save button is pressed, here is the image\'s URL on the device:', imagedata)


            window.imageResizer.resizeImage(
                function(data) {
                    //success
                    console.log('Image resizing success!', JSON.stringify(data));
                    //nowWeRun(jobPicFile)

                },
                function() {
                    //fail
                    console.log('Image resizing failure!', data)
                },
                imagedata,
                400,
                400, {
                    format: 'ImageResizer.FORMAT_JPG',
                    imageDataType: 'IMAGE_DATA_TYPE_URL',
                    resizeType: 'RESIZE_TYPE_PIXEL'
                }
            );


            //jobPicFile = new Parse.File(jobText + '-jobPic.jpg', imagedata, "image/jpeg");


            function nowWeRun(jobPicFile) {


                console.log('NOW WE RUN AWAY')

                /* jobPicFile.save().then(function () {
                    var job = new JobOb();
                    job.set("jobTitle", jobText);
                    console.log('set jobTitle as ' + jobText);
                    job.set("jobPic", jobPicFile);
                    console.log('jobPicFile url is ' + jobPicFile.url());
                    console.log('jobPicFile name is ' + jobPicFile.name());
                    job.set("jobDate", jobDate.toDate());
                    console.log('set jobDate as ' + jobDate.format("MMM DD, YYYY, HH:mm"));

                    //function is passed as a callback then called at the end of the 'getCoordinates' function ^_

                    getCoordinates(function (latitude, longitude) {
                        console.log('now lat and lng are ' + latitude, longitude);
                        job.set("jobCoord", new Parse.GeoPoint({
                            latitude: lat,
                            longitude: lng
                        }));
                        job.set("jobLocation", jobLocation);
                        console.log('jobLocation saved to parse is: ' + jobLocation);
                        job.save(null, {
                            success: function (ob) {
                                $.mobile.changePage("#feed");
                                console.log("SUCCESSFULLY UPLOADED JOB");
                                cleanUp();
                            },
                            error: function (e, error) {
                                console.log("Oh crap", JSON.stringify(error));
                                cleanUp();
                            }
                        });
                    });

                }, function (error) {
                    console.log("Error");
                    console.log(error);
                });*/
            }

            //This Code should save the information to Parse


        } else {

            var job = new JobOb();

            job.set("jobTitle", jobText);
            console.log(jobText);
            job.set("jobDate", jobDate.toDate());
            console.log(jobDate.toDate());

            $.mobile.loading("show");
            getCoordinates(function(latitude, longitude) {
                console.log('now lat and lng are ' + latitude, longitude);
                job.set("jobCoord", new Parse.GeoPoint({
                    latitude: lat,
                    longitude: lng
                }));

                job.set("jobLocation", jobLocation);

                console.log('jobLocation saved to parse is: ' + jobLocation);


                job.save(null, {
                    success: function(ob) {
                        $.mobile.changePage("#feed");
                        cleanUp();
                        console.log('cleaned up');
                    },
                    error: function(e, error) {
                        $.mobile.loading("hide");
                        console.log(JSON.stringify(error));
                        cleanUp();
                        console.log('cleaned up');
                    }
                });
                cleanUp();

            });
        }
    });



    // ************************************
    //  CONFIRMATION BOX ON ATTACHING PICTURES - Milly
    // ************************************

    function onConfirm(buttonIndex) {
        console.log('You selected button ' + buttonIndex);
        if (buttonIndex === 1) {
            console.log('you pressed take a photo');
            navigator.camera.getPicture(gotPic, failHandler, {
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.CAMERA,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 400,
                targetHeight: 400,
                quality: 50,
                saveToPhotoAlbum: false,
                correctOrientation: true
            });
        } else if (buttonIndex === 2) {
            console.log('you pressed attach a picture');
            navigator.camera.getPicture(gotPic, failHandler, {
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 400,
                targetHeight: 400,
                quality: 50,
                saveToPhotoAlbum: false,
                correctOrientation: true
            });
        }
    }


    $("#takePicBtn").on("click", function(e) {
        console.log('notfications should show up');
        navigator.notification.confirm(
            '', // message
            onConfirm, // callback to invoke with index of button pressed
            'Grab a Picture', // title
            ['Take a Photo', 'Attach a Picture'] // buttonLabels
        );
    });

    // ************************************
    //  END
    // ************************************






    function getCoordinates(callbackFunction) {
        navigator.geolocation.getCurrentPosition(
            function(location) {

                lat = location.coords.latitude;
                lng = location.coords.longitude;

                geocoder = new google.maps.Geocoder();

                var latlng = new google.maps.LatLng(lat, lng);

                geocoder.geocode({
                    'latLng': latlng
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[1]) {
                            //Grabs the suburb from the array address_components, and displays the short_name
                            jobLocation = JSON.stringify(results[1].address_components[0].short_name);
                            //console.log(JSON.stringify(results[1].address_components[0].short_name));
                            //infowindow.setContent(results[1].address_components[0].short_name);
                            console.log("jobLocation is: " + jobLocation);
                            /* console.log(lat);
                             console.log(lng);*/

                            callbackFunction(lat, lng);

                        } else {
                            alert('No results found');
                        }
                    } else {
                        alert('Geocoder failed due to: ' + status);
                    }

                });

            },
            function(error) {
                //error!
                console.log('couldnt get the location :( ');

            }
        );
        //grab my location!
    }



    // Success function for getting picture

    // *****************

    function gotPic(data) {
        // start loader here for pic
        $.mobile.loading("hide");
        console.log('got here');
        imagedata = data;
        console.log("File URI Comes from Cordova: " + data);
        //$("#takePicBtn").text("Picture Taken!").button("refresh").button("refresh");
        $("#takePicBtn").text("Picture Taken!");
    }

    //Fail handle for getting picture

    function failHandler(e) {
        console.log("error grabbing camera/gallery");
        //console.log(e);
        //console.log(e.toString());
    }


<<<<<<< HEAD:assets/www/js/main.js
=======
	// ************************************
	//  When you click the Save Job button
	// ************************************

	$("#saveJobBtn").on("click", function (e) {

		var jobDate = moment(),
			errors = '';

		console.log('Clicked the save job button.');

		e.preventDefault();

		//$(this).attr("disabled", "disabled").button("refresh");

		var jobText = $("#jobText").val();

		if ($('input[name="now_later"]:checked').val() === 'Later') {
			if (isValidDate() === true) {
				console.log('Save button: Custom date selected!');
				jobDate = getCustomJobDate();
				console.log('the custom date is: ', jobDate.format('DD/MM'))
			} else {

				//milly-to-read: you can replace this with the notification.alert popup from the phonegap docs:
				//http://docs.phonegap.com/en/2.9.0/cordova_notification_notification.md.html#notification.alert
				errors += 'Please select both a day and month.\n'
			}
		}

		if (jobText === '') {
			errors += 'Please write a job title. \n'
		}

		if (errors !== '') {
			alert(errors);
			return;
		}

 		if (imagedata !== "") {

		    /*
		    * milly-to-read
		    *
		    * So for the photo problem we need a way of reducing the file size of the image.
		    * We'll need a 'droid phonegap plugin for this. https://github.com/skilesare/PhoneGap-Image-Resizer
		    *
		    * */


		    var jobPicFile;

		    console.log('when the save button is pressed, here is the image\'s URL on the device:', imagedata)


		    window.imageResizer.resizeImage(
			    function(data) {
				    //success
				    console.log('Image resizing success!', JSON.stringify(data));
				    //nowWeRun(jobPicFile)

			    },
			    function () {
				    //fail
				    console.log('Image resizing failure!', data)
			    },
			    imagedata,
			    400,
			    400,
			    {
				    format:'jpg',
				    imageDataType: 'urlImage',
				    resizeType: 'pixelResize'
			    }
		    );


		    //jobPicFile = new Parse.File(jobText + '-jobPic.jpg', imagedata, "image/jpeg");


		    function nowWeRun(jobPicFile) {


			    console.log('NOW WE RUN AWAY')

			   /* jobPicFile.save().then(function () {
				    var job = new JobOb();
				    job.set("jobTitle", jobText);
				    console.log('set jobTitle as ' + jobText);
				    job.set("jobPic", jobPicFile);
				    console.log('jobPicFile url is ' + jobPicFile.url());
				    console.log('jobPicFile name is ' + jobPicFile.name());
				    job.set("jobDate", jobDate.toDate());
				    console.log('set jobDate as ' + jobDate.format("MMM DD, YYYY, HH:mm"));

				    //function is passed as a callback then called at the end of the 'getCoordinates' function ^_

				    getCoordinates(function (latitude, longitude) {
					    console.log('now lat and lng are ' + latitude, longitude);
					    job.set("jobCoord", new Parse.GeoPoint({
						    latitude: lat,
						    longitude: lng
					    }));
					    job.set("jobLocation", jobLocation);
					    console.log('jobLocation saved to parse is: ' + jobLocation);
					    job.save(null, {
						    success: function (ob) {
							    $.mobile.changePage("#feed");
							    console.log("SUCCESSFULLY UPLOADED JOB");
							    cleanUp();
						    },
						    error: function (e, error) {
							    console.log("Oh crap", JSON.stringify(error));
							    cleanUp();
						    }
					    });
				    });

			    }, function (error) {
				    console.log("Error");
				    console.log(error);
			    });*/
		    }

			//This Code should save the information to Parse


		} else {

			var job = new JobOb();

			job.set("jobTitle", jobText);
			console.log(jobText);
			job.set("jobDate", jobDate.toDate());
			console.log(jobDate.toDate());

		    $.mobile.loading("show");
			getCoordinates(function (latitude, longitude) {
				console.log('now lat and lng are ' + latitude, longitude);
				job.set("jobCoord", new Parse.GeoPoint({
					latitude: lat,
					longitude: lng
				}));

				job.set("jobLocation", jobLocation);

				console.log('jobLocation saved to parse is: ' + jobLocation);


				job.save(null, {
					success: function (ob) {
						$.mobile.changePage("#feed");
						cleanUp();
						console.log('cleaned up');
					},
					error: function (e, error) {
						$.mobile.loading("hide");
						console.log(JSON.stringify(error));
						cleanUp();
						console.log('cleaned up');
					}
				});
				cleanUp();

			});
		}
	});

	//THIS handles the taking a picture from camera or grabbing it from phone library
	$("#takePicBtn").on("click", function (e) {
		e.preventDefault();
		navigator.camera.getPicture(gotPic, failHandler, {
			destinationType: Camera.DestinationType.FILE_URI,
			sourceType: Camera.PictureSourceType.CAMERA,
			encodingType: Camera.EncodingType.JPEG,
			targetWidth: 400,
			targetHeight: 400,
			quality: 50,
			saveToPhotoAlbum: false,
			correctOrientation: true
		});
		$.mobile.loading("show");
	});

	function getCoordinates(callbackFunction) {
		navigator.geolocation.getCurrentPosition(
			function (location) {

				lat = location.coords.latitude;
				lng = location.coords.longitude;

				geocoder = new google.maps.Geocoder();

				var latlng = new google.maps.LatLng(lat, lng);

				geocoder.geocode({
					'latLng': latlng
				}, function (results, status) {
					if (status == google.maps.GeocoderStatus.OK) {
						if (results[1]) {
							//Grabs the suburb from the array address_components, and displays the short_name
							jobLocation = JSON.stringify(results[1].address_components[0].short_name);
							//console.log(JSON.stringify(results[1].address_components[0].short_name));
							//infowindow.setContent(results[1].address_components[0].short_name);
							console.log("jobLocation is: " + jobLocation);
							/* console.log(lat);
							 console.log(lng);*/

							callbackFunction(lat, lng);

						} else {
							alert('No results found');
						}
					} else {
						alert('Geocoder failed due to: ' + status);
					}

				});

			},
			function (error) {
				//error!
				console.log('couldnt get the location :( ');

			}
		);
		//grab my location!
	}


	//Debug Code for date buttons


	// *****************


	// Success function for getting picture

	function gotPic(data) {
		// start loader here for pic
		$.mobile.loading("hide");
		console.log('got here');
		imagedata = data;
		console.log("File URI Comes from Cordova: " + data);
		//$("#takePicBtn").text("Picture Taken!").button("refresh").button("refresh");
		$("#takePicBtn").text("Picture Taken!");
	}

	//Fail handle for getting picture

	function failHandler(e) {
		console.log("error grabbing camera/gallery");
		//console.log(e);
		//console.log(e.toString());
	}

	//Resets all buttons on Save

	function cleanUp() {
		imagedata = "";
		lat = 0;
		lng = 0;
		jobLocation = "";
		//$("#saveJobBtn").removeAttr("disabled").button("refresh");
		$("#jobText").val("");
		//$("#takePicBtn").text("Add Pic").button("refresh");

	}
>>>>>>> ec3f91cb0e0e8e691e065d9e3e70f1e82408bac8:assets/www/js/view/main.js
});

//Resets all buttons on Save

function cleanUp() {
    imagedata = "";
    lat = 0;
    lng = 0;
    jobLocation = "";
    //$("#saveJobBtn").removeAttr("disabled").button("refresh");
    $("#jobText").val("");
    //$("#takePicBtn").text("Add Pic").button("refresh");

}



// ************************************
//  RADIUS IN KM QUERIES - Milly
// ************************************

$('li.filterBtn > button').click(function() {

//the + symbol changes the value into a number
    var _val = +($(this).val());
    console.log(_val);
    if ((_val === 1) || (_val === 3) || (_val === 5)) {
        console.log('if statement fired');

        navigator.geolocation.getCurrentPosition(
            function(location) {

                lat = location.coords.latitude;
                lng = location.coords.longitude;
                changeDistance(_val);
                console.log('radius is ' + _val);

            });
    } else if ($(this).val() === 'all') {
        console.log("window is now refreshed");
        window.location = 'index.html#feed';
    }



});


function changeDistance(distance) {
    console.log('distance is' + distance);

    $.mobile.loading("show");

    //Parse query to get results
    var query = new Parse.Query(JobOb);
    var point = new Parse.GeoPoint(lat, lng);
    console.log(point);



    query.limit(10);
    query.withinKilometers("jobCoord", point, distance);


    query.find({
        success: function(results) {
            $.mobile.loading("hide");

            var s = "";
            var shortDate = moment();
            for (var i = 0; i < results.length; i++) {
                //Inputting the HTML into #feed div[data-role=feed-content]
                // Including the job title, job Date, Picture and offer help button
                s += "<div class='job-details'>";

                //Job title
                s += "<p class='job-text'>" + "'" + results[i].get("jobTitle") + "'" + "</p>";

                //Job Picture
                var pic = results[i].get("jobPic");
                if (pic) {
                    s += "<div class='jobPicture'>" + "<img src='" + pic.url() + "'>" + "</div>";
                }

                //Job Date
                shortDate = moment(results[i].get("jobDate"));
                s += "<h6 class='jobTimeText'>" + shortDate.format("DD/MM") + "</h6>";


                s += "<h6 class='jobLocationText'>" + results[i].get("jobLocation") + "</h6>";
                //End DIV
                s += "</div>";

                //console.log('shortDate is: ' + shortDate.format("DD/MM"));

            }
            $("#feed div[data-role=feed-content]").html(s);

        },
        //if Something goes wrong
        error: function(e, error) {
            $.mobile.loading("hide");
            console.log(e);
        }
    });

}
// ************************************
//  END
// ************************************


//}
