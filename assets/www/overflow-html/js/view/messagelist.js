jQuery.extend(true, app, {
	messagelist: {
		init: function () {
			console.log('Message list loaded');

			var _self = this;

			$(document).on("pagecreate", '#messagelist', function () {
				console.log('pagecreate: #messagelist: This should fire only once.');
				if (app.authentication.is_logged_in() === false) {
					$.mobile.navigate('#home');
				} else {
					_self.listeners();
				}

			});

			$(document).on("pagebeforeshow", '#messagelist', function () {
				if (app.authentication.is_logged_in() === false) {
					$.mobile.navigate('#home');
				} else {
					//now we refresh the list plz ok!
				}
			});

		},
		listeners: function () {

		}
	}
});

