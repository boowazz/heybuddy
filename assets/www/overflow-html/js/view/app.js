//let's define some constants!
var app = {
    parse_app_id: "o6UmKN6Q81i9tHglqCtWVWMbz437ssAzNLrWuvwI",
    parse_js_id: "0HMBdVJgrpYOc0yAQDdUXFQqMtLMB3DPOAhMEOND",
	parse_client_key: "yJFQsCi35UMsy7HTzPwROk7CtisXhbvtMJ8dSnG9",
    fb_app_id: "717765138254866",
    phono_api_key: "bc1cdeaaab263d8ee2ec6eb6c26bdf1d",
	parse_master_key: "yUrAuFYaNnhBDERVawdCWK7bgXRHYJqKvVEojdFJ",
	name: 'HeyBuddy',
	globals: function () {

		var _self = this;
		console.log('establish globals')

		//setup Parse. If we gon' push, how we gon'
		Parse.initialize(app.parse_app_id, app.parse_js_id);

		$.mobile.loader.prototype.options.text = "loading";
		$.mobile.loader.prototype.options.textVisible = false;
		$.mobile.loader.prototype.options.theme = "a";
		$.mobile.loader.prototype.options.html = "";


		_self.date_formats = {
			default: "DD/MM/YY",
			long: "dddd DD.MM.YYYY HH:mm"
		};

		Handlebars.registerHelper("formatDate", function(datetime, format) {
			if (moment) {
				var f =_self.date_formats[format];
				return moment(datetime).format(f);
			}
			else {
				return datetime;
			}
		});

		$('.js-logout').on('click', function (e) {
			e.preventDefault();
			app.authentication.logout();
		});

		if (app.on_mobile() === true) {
			window.plugins.webintent.hasExtra(
				"com.parse.Data",
				function(has) {
					if(has) {
						window.plugins.webintent.getExtra("com.parse.Data",
							function(d) {
								console.log(JSON.stringify(d));
							},
							function() {
								navigator.notification.alert(
									'Sorry, that notification didn\'t work. It was probably a message, head to your messages page!',
									null,
									app.name,
									'Ok'
								);
							}
						)
					}
				}
			)
		}


	},


	on_mobile: function () {

		if (typeof device !== 'undefined') {
			return true
		} else {
			return false;
		}

	},

	notification: function (message,callback) {
		if (app.on_mobile() === true) {
			navigator.notification.alert(
				message,  // message
				callback,         // callback
				app.name,            // title
				'ok'                  // buttonName
			);
		} else {
			alert(message)
		}
	},
};

var ua = navigator.userAgent.toLowerCase();
var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
if(isAndroid) {
    // Do something!
    // Redirect to Android-site?
    var deviceReadyDeferred = $.Deferred();
    var jqmReadyDeferred = $.Deferred();

    document.addEventListener("deviceReady", deviceReady, false);

     function deviceReady() {
        deviceReadyDeferred.resolve();
     }

    $(document).one("mobileinit", function () {
        jqmReadyDeferred.resolve();
    });

    $.when(deviceReadyDeferred, jqmReadyDeferred).then(allReady);

} else {
    var jqmReadyDeferred = $.Deferred();


    $(document).one("mobileinit", function () {
        jqmReadyDeferred.resolve();
    });

    $.when(jqmReadyDeferred).then(allReady);
}

function allReady() {

    $('.opacity-wrapper').addClass('loaded check');


    $.extend(true, app, {
        home: {

	        init: function () {

	        },

            messaging: function () {
                var audioType;
                if (navigator.javaEnabled()) {
                    console.log('java enabled');
                    audioType = 'java';
                } else {
                    audioType = 'none';
                }
                $.phono({
                    apiKey: app.phono_api_key,
                    audio: {
                        type: 'none'
                    },
                    onReady: function (event) {
                        console.log("My XMPP address is " + this.sessionId);
                    },
                    messaging: {
                        onMessage: function (event) {
                            var message = event.message;
                            console.log("Message from: " + message.from + " " + message.body);
                        }
                    }
                });
            }
        }
    });

    app.globals();

	try{
		app.home.init();
	} catch (e) {
		console.log('home trouble', e)
	}
	try{
		app.authentication.init();
	} catch (e) {
		console.log('auth trouble', e)
	}
	try{
		app.feed.init();
	} catch (e) {
		console.log('feed trouble', e)
	}
	try{
		app.addjob.init();
	} catch (e) {
		console.log('addjob trouble', e)
	}

	try{
		app.messagelist.init();
	} catch (e) {
		console.log('messagelist trouble', e)
	}

	if ((app.authentication.is_logged_in() === true) && ((location.hash === '#home') || (location.hash === ''))) {
		$.mobile.navigate('#feed');
	}

}


