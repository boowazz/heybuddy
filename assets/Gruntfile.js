/*
* milly-to-read
*
* This is my personal preference for getting the scss compiling into CSS.
* The setup is outlined in the readme. Essentially you don't have to use this at all!
* I assume you're using something else like the ruby compiler which is totally fine.
* */

module.exports = function (grunt) {

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        sass: {
            dev: {
                options: {
                    //includePaths: require('node-bourbon').includePaths,
                    outputStyle: 'expanded',
                    sourceComments: 'map',
                    sourceMap: './www/css/index.css.map'
                },
                files: {
                    './www/css/index.css': './www/css/index.scss'
                }
            },
            build: {
                options: {
                    //includePaths: require('node-bourbon').includePaths,
                    outputStyle: 'compressed'
                },
                files: {
                    './www/css/index.css': './www/css/index.scss'
                }
            }
        },

        watch: {
            options: {
                livereload: true
            },
            sass: {
                options: {
                    livereload: false
                },

                files: [
                    './www/css/**/*.scss'
                ],
                tasks: ['sassdev']
            },
            css: {
                files: ['./www/css/index.css'],
                tasks:[]
            },
	        template: {
		        files: ['./www/index.html'],
		        tasks:[]
	        },
	        js: {
		        files: ['./www/js/view/**/*.js'],
		        tasks: []
	        }
        }
    });

    grunt.registerTask('message:watch', [], function () {
        grunt.log.writeln([
            '\n*************\n'['rainbow'].bold +
            'THE SCSS IS NOW BEING WATCHED FOR CHANGES \n'['green'].bold +
            '*************'['rainbow'].bold
        ]);
    });

    grunt.registerTask('message:build', [], function () {
        grunt.log.writeln([
            '\n*************\n'['rainbow'].bold +
                'THE SCSS HAS NOW BEEN COMPILED TO COMPRESSED CSS \n'['green'].bold +
                '*************'['rainbow'].bold
        ]);
    });

    grunt.registerTask('sassdev', [], function () {
        grunt.loadNpmTasks('grunt-sass');
        grunt.task.run('sass:dev');
    });

    grunt.registerTask('sassbuild', [], function () {
        grunt.loadNpmTasks('grunt-sass');
        grunt.task.run('sass:build');
    });

    grunt.registerTask('default', [], function () {
        grunt.loadNpmTasks('grunt-contrib-watch');
        grunt.task.run('sassdev','message:watch', 'watch');
    });

    grunt.registerTask('build', [], function () {
        grunt.task.run('message:build','sassbuild');
    });


};