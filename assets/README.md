#Developing

###NPM packages

If you already have nvm (node version manager), an active NPM installation and grunt-cli installed, you won't need to do this.

1. Install [NVM](https://github.com/creationix/nvm)
2. `nvm install 0.10`
3. `nvm alias default 0.10`
4. `source ~/.bash_profile`
5. `npm install -g grunt-cli` ; Install Grunt CLI tools globally

###Set up development

1. from the root: `cd assets`
2. `npm install`

###Developing

1. from the root: `cd assets`
2. `grunt` (you can see better terminal logs with `grunt --verbose`)

###Building
1. `cd assets`
2. `grunt build` (you can see better terminal logs with `grunt build --verbose`)

#Working

- Send push notification to user (will fake this)

#Still to do:

##Messaging

http://www.raymondcamden.com/index.cfm/2012/10/10/PhoneGap-Parsecom-and-Push-Notifications

Pushing is now working I think?


###Selecting a job
1. Use the job's ID to look up the user
2. Establish a fresh session via Phono
3. Send a push notification to the job creator that contains:
	1. Phono Session ID
	2. Job object ID
	3. User ID
4. Job thread created on the messages page.
5. Now the thread has a session we wait for the answer.

###Answering a push notification
1. Clicking on a push loads up the app
2. Read the session ID from the push via webIntents
3. Establish a fresh session via Phono
4. Begin chat
	- **It would be great to do a push notification here**

###Marking a job as complete.
1. Go to job list OR chat thread related to job.
2. Click 'Complete' to complete the job.
3. The job object has a 'completed_by' field that is filled in with the requisite user.

##History
- Shows a list of recently created or completed jobs, chats (from localstorage)
- Non interactive
